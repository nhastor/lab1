package ba.unsa.etf.rma.vj_18352;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MovieDetailList extends AppCompatActivity {
    private Movie movie;

    public MovieDetailList(Movie movie) {
        this.movie = movie;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_detail_list);

        ((TextView) findViewById(R.id.textViewTitle)).setText((CharSequence) "Title:");
        ((TextView) findViewById(R.id.textViewGenre)).setText((CharSequence) "Genre:");
        ((TextView) findViewById(R.id.textViewReleaseDate)).setText((CharSequence) "Release date:");
        ((TextView) findViewById(R.id.textViewHomePage)).setText((CharSequence) "Home page:");
        ((TextView) findViewById(R.id.textViewOverview)).setText((CharSequence) "Overview:");
        ((TextView) findViewById(R.id.textViewActors)).setText((CharSequence) "Actors:");

        ((TextView) findViewById(R.id.textViewMovieTitle)).setText(movie.getTitle());
        ((TextView) findViewById(R.id.textViewMovieGenre)).setText(movie.getGenre());
        ((TextView) findViewById(R.id.textViewMovieReleaseDate)).setText(movie.getReleaseDate());
        ((TextView) findViewById(R.id.textViewMovieHomePage)).setText(movie.getHomepage());
        ((TextView) findViewById(R.id.textViewMovieOverview)).setText(movie.getOverview());

        ((TextView) findViewById(R.id.textViewActor1)).setText((CharSequence) "Actor1");
        ((TextView) findViewById(R.id.textViewActor1)).setText((CharSequence) "Actor2");
        ((TextView) findViewById(R.id.textViewActor1)).setText((CharSequence) "Actor3");
        ((TextView) findViewById(R.id.textViewActor1)).setText((CharSequence) "Actor4");
        ((TextView) findViewById(R.id.textViewActor1)).setText((CharSequence) "Actor5");


    }

    }
