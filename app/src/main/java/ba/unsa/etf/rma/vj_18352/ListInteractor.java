package ba.unsa.etf.rma.vj_18352;

import java.util.List;

public interface ListInteractor {
    List<Movie> get();
}
