package ba.unsa.etf.rma.vj_18352;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class MovieListAdapter extends ArrayAdapter<Movie> {
    private int resource;
    private Context context;

    public MovieListAdapter(@NonNull Context context, int _resource, List<Movie> items) {
        super(context, _resource, items);
        resource = _resource;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if(convertView == null){
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater) getContext().getSystemService(inflater);
            li.inflate(resource, newView, true);
        }
        else{
            newView = (LinearLayout) convertView;
        }
        Movie movie = getItem(position);

        if(movie==null)
            return newView;

        TextView name = (TextView) newView.findViewById(R.id.itemName);
        TextView genre = (TextView) newView.findViewById(R.id.itemGenre);
        ImageView icon = (ImageView) newView.findViewById(R.id.icon);

        name.setText(movie.getTitle());
        genre.setText(movie.getGenre());

        switch (movie.getGenre().toLowerCase()){
            case "crime":
                icon.setImageResource(R.drawable.crime);
                break;
            case "mystery":
                icon.setImageResource(R.drawable.mystery);
                break;
            case "thriller":
                icon.setImageResource(R.drawable.thriller);
                break;
            case "horror":
                icon.setImageResource(R.drawable.horror);
                break;
            case "comedy":
                icon.setImageResource(R.drawable.comedy);
                break;
            case "drama":
                icon.setImageResource(R.drawable.drama);
                break;
            case "romance":
                icon.setImageResource(R.drawable.romance);
                break;
            default:
                icon.setImageResource(R.drawable.picture1);
        }


        return newView;
    }
}
