package ba.unsa.etf.rma.vj_18352;

import java.util.ArrayList;
import java.util.List;

public class MoviesModel {
    public static List<Movie> movies;

    public static List<Movie> getMovies() {
        if(movies==null) {
            movies = new ArrayList<>();
            movies.add(new Movie("Se7en", "Thriller", "1995", "https://www.imdb.com/title/tt0114369/", "Najbolji film"));
            movies.add(new Movie("The Godfather", "Crime", "1972", "https://www.imdb.com/title/tt0068646/", "Najbolji film"));
            movies.add(new Movie("The game", "Mystery", "1997", "https://www.imdb.com/title/tt0119174/", "3. Najbolji film"));
        }
        return movies;
    }

    public static void setMovies(List<Movie> movies) {
        MoviesModel.movies = movies;
    }
}
